package it.com.atlassian.oauth;

import org.apache.commons.codec.binary.Base64;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.net.URLEncoder;
import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.Signature;
import java.security.spec.KeySpec;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertEquals;

/**
 * A counterpart to {@link OAuth2LOTest} that uses HTTP directly without going via {@link net.oauth.client.OAuthClient}.
 * This may make it clearer what's passing over the wire.
 */
public class OAuth2LODirectHttpTest {
    private CloseableHttpClient client;

    @Before
    public void setUp() {
        client = HttpClientBuilder.create().build();
    }

    @After
    public void shutdown() throws IOException {
        client.close();
    }

    @Test
    public void getWithoutOAuthParametersFails() throws IOException {
        final HttpGet get = new HttpGet(OAuthTestBase.BASE_URL + "/plugins/servlet/oauth/integration-test/strict-security");

        final HttpResponse response = client.execute(get);

        assertEquals(HttpStatus.SC_UNAUTHORIZED, response.getStatusLine().getStatusCode());
    }

    @Test
    public void getWithHandmadeOAuthParametersSucceeds() throws Exception {
        String method = "GET";
        String resourceUrl = OAuthTestBase.BASE_URL + "/plugins/servlet/oauth/integration-test/strict-security";

        SortedMap<String, String> params = new TreeMap<String, String>();
        params.put("oauth_consumer_key", "hardcoded-2lo-consumer");
        params.put("oauth_token", "");
//        params.put("oauth_signature_method", "HMAC-SHA1");
        params.put("oauth_signature_method", "RSA-SHA1");
        params.put("oauth_timestamp", Long.toString(TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis())));
        params.put("oauth_nonce", "random-nonce-" + Math.random());
        params.put("oauth_version", "1.0");

        params.put("param", "value");

        StringBuilder sb = new StringBuilder();
        for (Map.Entry<String, String> e : params.entrySet()) {
            if (sb.length() > 0) {
                sb.append("&");
            }

            sb.append(e.getKey());
            sb.append('=');
            sb.append(e.getValue());
        }

        String signatureBaseString = method
                + "&" + URLEncoder.encode(resourceUrl, "UTF-8")
                + "&" + URLEncoder.encode(sb.toString(), "UTF-8");

//        System.out.println(URLEncoder.encode(signatureBaseString, "UTF-8"));

        KeyFactory kf = KeyFactory.getInstance("RSA");
        KeySpec privateKeySpec = new PKCS8EncodedKeySpec(Base64.decodeBase64(OAuthTestBase.CONSUMER_PRIVATE_KEY.getBytes("UTF-8")));

        PrivateKey pk = kf.generatePrivate(privateKeySpec);

        Signature instance = Signature.getInstance("SHA1withRSA");
        instance.initSign(pk);
        instance.update(signatureBaseString.getBytes("UTF-8"));
        byte[] signature = instance.sign();

        String signatureStr = new String(Base64.encodeBase64(signature), "UTF-8");

        String paramString = sb + "&oauth_signature=" + URLEncoder.encode(signatureStr, "UTF-8");

        final HttpGet get = new HttpGet(resourceUrl + "?" + paramString);

        final HttpResponse response = client.execute(get);

        assertEquals(HttpStatus.SC_OK, response.getStatusLine().getStatusCode());
    }
}
