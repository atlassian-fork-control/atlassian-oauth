package com.atlassian.oauth.consumer.sal;

import com.atlassian.oauth.util.RSAKeys;

import java.security.GeneralSecurityException;
import java.security.KeyPair;

/**
 * Simple implementation that uses the installed crypto provider to generate RSA key pairs.
 */
public class KeyPairFactoryImpl implements KeyPairFactory {
    public KeyPair newKeyPair() throws GeneralSecurityException {
        return RSAKeys.generateKeyPair();
    }
}
