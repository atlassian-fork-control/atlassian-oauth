package com.atlassian.oauth.shared.servlet;

import org.junit.Before;
import org.junit.Test;

import java.util.Properties;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

public class HelpLinkResolverTest {
    private static final String BASE_URL = "http://example.com/";
    private static final String DEFAULT_PAGE = "default";
    private static final String DEFAULT_LINK = BASE_URL + DEFAULT_PAGE;

    private HelpLinkResolver resolver;

    @Before
    public void setUp() throws Exception {
        Properties properties = new Properties();
        properties.put("base.url", BASE_URL);
        properties.put("default.page", DEFAULT_PAGE);
        resolver = new HelpLinkResolver(properties);
    }

    @Test
    public void assertDefaultLinkUsedOnUndefinedHelpKey() {
        assertThat(resolver.getLink("__badkey__"), is(equalTo(DEFAULT_LINK)));
    }

    @Test
    public void assertDefaultLinkUsedOnEmptyKey() {
        assertThat(resolver.getLink(""), is(equalTo(DEFAULT_LINK)));
    }

    @Test(expected = NullPointerException.class)
    public void assertNullPointExceptionOnNullKey() {
        resolver.getLink(null);
    }
}
