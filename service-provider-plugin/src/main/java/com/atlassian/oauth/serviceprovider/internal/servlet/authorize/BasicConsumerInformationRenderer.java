package com.atlassian.oauth.serviceprovider.internal.servlet.authorize;

import com.atlassian.oauth.serviceprovider.ConsumerInformationRenderException;
import com.atlassian.oauth.serviceprovider.ConsumerInformationRenderer;
import com.atlassian.oauth.serviceprovider.ServiceProviderToken;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.sal.api.user.UserProfile;
import com.atlassian.templaterenderer.RenderingException;
import com.atlassian.templaterenderer.TemplateRenderer;
import com.google.common.collect.ImmutableMap;
import org.apache.commons.lang.StringUtils;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.io.Writer;
import java.net.URI;
import java.util.Map;

import static com.google.common.base.Preconditions.checkNotNull;


public class BasicConsumerInformationRenderer implements ConsumerInformationRenderer {
    private static final String TEMPLATE = "templates/auth/basic-consumer-info.vm";

    private final ApplicationProperties applicationProperties;
    private final TemplateRenderer renderer;
    private final UserManager userManager;

    public BasicConsumerInformationRenderer(ApplicationProperties applicationProperties,
                                            TemplateRenderer renderer, UserManager userManager) {
        this.applicationProperties = checkNotNull(applicationProperties, "renderer");
        this.renderer = checkNotNull(renderer, "renderer");
        this.userManager = checkNotNull(userManager, "userManager");
    }

    public boolean canRender(ServiceProviderToken token, HttpServletRequest request) {
        return true;
    }

    public void render(ServiceProviderToken token, HttpServletRequest request, Writer writer) throws IOException {
        URI appUri = URI.create(applicationProperties.getBaseUrl());

        String userFullName;
        String username = userManager.getRemoteUsername();
        if (StringUtils.isNotBlank(username)) {
            UserProfile profile = userManager.getUserProfile(username);
            if (profile != null && StringUtils.isNotBlank(profile.getFullName())) {
                userFullName = profile.getFullName();
            } else {
                userFullName = username;
            }
        } else {
            userFullName = "User unknown";
        }

        Map<String, Object> context = ImmutableMap.<String, Object>of(
                "consumer", token.getConsumer(),
                "applicationDomain", appUri.getHost(),
                "userFullName", userFullName);

        try {
            renderer.render(TEMPLATE, context, writer);
        } catch (RenderingException e) {
            throw new ConsumerInformationRenderException("Could not render consumer information", e);
        }
    }
}
