package com.atlassian.oauth.serviceprovider.internal;

import net.oauth.OAuth;
import net.oauth.OAuthAccessor;
import net.oauth.OAuthException;
import net.oauth.OAuthMessage;
import net.oauth.OAuthProblemException;
import net.oauth.OAuthValidator;
import net.oauth.SimpleOAuthValidator;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.concurrent.TimeUnit;

import static net.oauth.OAuth.OAUTH_SIGNATURE_METHOD;
import static net.oauth.OAuth.Problems.SIGNATURE_METHOD_REJECTED;
import static net.oauth.OAuth.RSA_SHA1;

/**
 * A validator that wraps an instance of the {@link SimpleOAuthValidator} and verifies that the OAuth signature method
 * is RSA_SHA1.
 */
public class OAuthValidatorImpl implements OAuthValidator {
    private InMemoryNonceService nonceServiceService = new InMemoryNonceService(TimeUnit.MILLISECONDS.toSeconds(SimpleOAuthValidator.DEFAULT_TIMESTAMP_WINDOW));

    private OAuthValidator simpleValidator = new SimpleOAuthValidator(SimpleOAuthValidator.DEFAULT_TIMESTAMP_WINDOW, Double.parseDouble(OAuth.VERSION_1_0)) {
        @Override
        protected void validateTimestampAndNonce(OAuthMessage message) throws IOException, OAuthProblemException {
            super.validateTimestampAndNonce(message);

            // Both timestamp and nonce params cannot be null since they have been checked in {@link SimpleOAuthValidator#validateTimestampAndNonce}.
            final long timestamp = Long.parseLong(message.getParameter(OAuth.OAUTH_TIMESTAMP));
            final String nonce = message.getParameter(OAuth.OAUTH_NONCE);
            nonceServiceService.validateNonce(message.getConsumerKey(), timestamp, nonce);
        }
    };

    public void validateMessage(OAuthMessage message, OAuthAccessor accessor)
            throws OAuthException, IOException, URISyntaxException {
        message.requireParameters(OAUTH_SIGNATURE_METHOD, OAuth.OAUTH_CONSUMER_KEY);
        if (!message.getParameter(OAUTH_SIGNATURE_METHOD).equals(RSA_SHA1)) {
            throw new OAuthProblemException(SIGNATURE_METHOD_REJECTED);
        }
        simpleValidator.validateMessage(message, accessor);
    }


}