package com.atlassian.oauth.serviceprovider.internal;

import com.atlassian.oauth.Request;
import com.atlassian.oauth.Request.HttpMethod;
import com.atlassian.oauth.Request.Parameter;
import com.atlassian.oauth.serviceprovider.TokenPropertiesFactory;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.net.URI;
import java.util.Map;

import static com.atlassian.oauth.testing.Matchers.hasEntries;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class AggregateTokenPropertiesFactoryTest {
    static final Map<String, String> FIRST_PROPERTIES = ImmutableMap.of("first_property", "first_value");
    static final Map<String, String> SECOND_PROPERTIES = ImmutableMap.of("second_property", "second_value");
    static final Map<String, String> THIRD_PROPERTIES = ImmutableMap.of("third_property", "third_value");
    static final Map<String, String> ALL_PROPERTIES = ImmutableMap.<String, String>builder()
            .putAll(FIRST_PROPERTIES)
            .putAll(SECOND_PROPERTIES)
            .putAll(THIRD_PROPERTIES)
            .build();

    @Mock
    TokenPropertiesFactory first;
    @Mock
    TokenPropertiesFactory second;
    @Mock
    TokenPropertiesFactory third;

    TokenPropertiesFactory propertiesFactory;

    Request request = new Request(HttpMethod.GET, URI.create("http://example.com"), ImmutableList.<Parameter>of());

    @Before
    public void setUp() {
        propertiesFactory = new AggregateTokenPropertiesFactory(ImmutableList.of(first, second, third));
    }

    @Test
    public void assertThatPropertiesFromAllFactoriesAreCombined() {
        when(first.newRequestTokenProperties(request)).thenReturn(FIRST_PROPERTIES);
        when(second.newRequestTokenProperties(request)).thenReturn(SECOND_PROPERTIES);
        when(third.newRequestTokenProperties(request)).thenReturn(THIRD_PROPERTIES);

        Map<String, String> tokenProperties = propertiesFactory.newRequestTokenProperties(request);

        assertThat(tokenProperties, allOf(hasEntries(FIRST_PROPERTIES), hasEntries(SECOND_PROPERTIES), hasEntries(THIRD_PROPERTIES)));
    }

    @Test
    public void assertThatServiceUnavailableExceptionCausesPropertiesFactoryToBeSkipped() {
        when(first.newRequestTokenProperties(request)).thenReturn(FIRST_PROPERTIES);
        when(second.newRequestTokenProperties(request)).thenThrow(new ServiceUnavailableException());
        when(third.newRequestTokenProperties(request)).thenReturn(THIRD_PROPERTIES);

        Map<String, String> tokenProperties = propertiesFactory.newRequestTokenProperties(request);

        assertThat(tokenProperties, allOf(hasEntries(FIRST_PROPERTIES), hasEntries(THIRD_PROPERTIES)));
    }

    private static class ServiceUnavailableException extends RuntimeException {
    }
}
