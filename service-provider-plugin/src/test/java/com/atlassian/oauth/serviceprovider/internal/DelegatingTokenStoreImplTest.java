package com.atlassian.oauth.serviceprovider.internal;

import com.atlassian.oauth.serviceprovider.InvalidTokenException;
import com.atlassian.oauth.serviceprovider.ServiceProviderTokenStore;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class DelegatingTokenStoreImplTest {
    private static final String TOKEN = "1234";

    @Mock
    ServiceProviderTokenStore delegateTokenStore;

    ServiceProviderTokenStore store;

    @Before
    public void setUp() {
        store = new DelegatingTokenStoreImpl(delegateTokenStore);
    }

    @Test(expected = InvalidTokenException.class)
    public void verifyThatTokenIsRemovedWhenInvalidTokenExceptionIsThrownByGet() {
        when(delegateTokenStore.get(TOKEN)).thenThrow(new InvalidTokenException("Invalid token"));

        try {
            store.get(TOKEN);
        } finally {
            verify(delegateTokenStore).removeAndNotify(TOKEN);
        }
    }
}
