package com.atlassian.oauth.serviceprovider.internal.servlet;

import com.atlassian.oauth.serviceprovider.internal.OAuthProblem;
import com.atlassian.oauth.serviceprovider.internal.util.UserAgentUtil;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.auth.AuthenticationController;
import com.atlassian.sal.api.auth.AuthenticationListener;
import com.atlassian.sal.api.auth.Authenticator;
import com.atlassian.sal.api.auth.Authenticator.Result;
import com.atlassian.sal.api.auth.OAuthRequestVerifier;
import com.atlassian.sal.api.auth.OAuthRequestVerifierFactory;
import com.google.common.collect.ImmutableMap;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Map;

import static com.atlassian.oauth.testing.TestData.USER;
import static net.oauth.OAuth.OAUTH_CONSUMER_KEY;
import static net.oauth.OAuth.OAUTH_NONCE;
import static net.oauth.OAuth.OAUTH_SIGNATURE;
import static net.oauth.OAuth.OAUTH_SIGNATURE_METHOD;
import static net.oauth.OAuth.OAUTH_TIMESTAMP;
import static net.oauth.OAuth.OAUTH_TOKEN;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.isA;
import static org.mockito.Matchers.startsWith;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class OAuthFilterTest {
    private static final Map<String, String[]> OAUTH_PARAMETERS = ImmutableMap.<String, String[]>builder()
            .put(OAUTH_CONSUMER_KEY, new String[]{"consumer-key"})
            .put(OAUTH_TOKEN, new String[]{"1234"})
            .put(OAUTH_SIGNATURE_METHOD, new String[]{"RSA-SHA1"})
            .put(OAUTH_SIGNATURE, new String[]{"signature"})
            .put(OAUTH_TIMESTAMP, new String[]{"1111111111"})
            .put(OAUTH_NONCE, new String[]{"1"})
            .build();


    @Mock
    Authenticator authenticator;
    @Mock
    AuthenticationListener authenticationListener;
    @Mock
    AuthenticationController authenticationController;
    @Mock
    ApplicationProperties applicationProperties;
    OAuthRequestVerifierFactory verifierFactory = new OAuthRequestVerifierFactory() {
        public OAuthRequestVerifier getInstance(ServletRequest req) {
            return new OAuthRequestVerifier() {

                boolean val = false;

                @Override
                public boolean isVerified() {
                    return val;
                }

                @Override
                public void setVerified(boolean b) {
                    val = b;
                }

                @Override
                public void clear() {
                    val = false;
                }
            };
        }
    };

    Filter filter;

    @Mock
    HttpServletRequest request;
    @Mock
    HttpServletResponse response;
    @Mock
    FilterChain chain;

    @Before
    public void setUp() {
        filter = new OAuthFilter(authenticator, authenticationListener, authenticationController, applicationProperties, verifierFactory);
        when(request.getRequestURL()).thenReturn(new StringBuffer("/this/is/resource"));
    }

    @Test
    public void verifyThatWhenAccessingANonProtectedResourceWeJustLetTheRequestPassThru() throws Exception {
        when(authenticationController.shouldAttemptAuthentication(request)).thenReturn(false);

        filter.doFilter(request, response, chain);

        verify(chain).doFilter(isA(HttpServletRequest.class), isA(HttpServletResponse.class));
        verify(authenticationListener).authenticationNotAttempted(isA(HttpServletRequest.class), isA(HttpServletResponse.class));
        verifyNoMoreInteractions(authenticationListener);
        verifyZeroInteractions(authenticator);
    }

    @Test
    public void verifyThatWhenOAuthParametersAreNotPresentWeLetTheRequestPassThru() throws Exception {
        when(authenticationController.shouldAttemptAuthentication(request)).thenReturn(true);

        filter.doFilter(request, response, chain);

        verify(chain).doFilter(isA(HttpServletRequest.class), isA(HttpServletResponse.class));
        verify(response, never()).addHeader(eq("WWW-Authenticate"), startsWith("OAuth"));
        verify(authenticationListener).authenticationNotAttempted(isA(HttpServletRequest.class), isA(HttpServletResponse.class));
        verifyNoMoreInteractions(authenticationListener);
        verifyZeroInteractions(authenticator);
    }

    @Test
    public void verifyWWWAuthenticateHeaderAddedWhenStatusIsSetToUnauthorizedWithoutAMessage() throws Exception {
        when(authenticationController.shouldAttemptAuthentication(request)).thenReturn(false);
        doAnswer(new FilterChainInvocation() {
            protected void chainInvoked(HttpServletRequest request, HttpServletResponse response) {
                response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            }
        }).when(chain).doFilter(isA(HttpServletRequest.class), isA(HttpServletResponse.class));

        filter.doFilter(request, response, chain);

        verify(response).addHeader(eq("WWW-Authenticate"), startsWith("OAuth"));
    }

    @Test
    public void verifyWWWAuthenticateHeaderNotAddedWhenStatusIsSetToUnauthorizedWithMacFinder() throws Exception {
        when(authenticationController.shouldAttemptAuthentication(request)).thenReturn(false);
        when(request.getHeader(UserAgentUtil.HEADER_USER_AGENT)).thenReturn("WebDAVLib/1.3");
        doAnswer(new FilterChainInvocation() {
            protected void chainInvoked(HttpServletRequest request, HttpServletResponse response) {
                response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            }
        }).when(chain).doFilter(isA(HttpServletRequest.class), isA(HttpServletResponse.class));

        filter.doFilter(request, response, chain);

        verify(response, never()).addHeader(eq("WWW-Authenticate"), startsWith("OAuth"));
    }

    @Test
    public void verifyWWWAuthenticateHeaderNotAddedWhenStatusIsSetToUnauthorizedWithWindowNetworkDriver() throws Exception {
        when(authenticationController.shouldAttemptAuthentication(request)).thenReturn(false);
        when(request.getHeader(UserAgentUtil.HEADER_USER_AGENT)).thenReturn("Microsoft-WebDAV-MiniRedir/6.1.7600");
        doAnswer(new FilterChainInvocation() {
            protected void chainInvoked(HttpServletRequest request, HttpServletResponse response) {
                response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            }
        }).when(chain).doFilter(isA(HttpServletRequest.class), isA(HttpServletResponse.class));

        filter.doFilter(request, response, chain);

        verify(response, never()).addHeader(eq("WWW-Authenticate"), startsWith("OAuth"));
    }

    @Test
    public void verifyWWWAuthenticateHeaderAddedWhenStatusIsSetToUnauthorizedWithAMessage() throws Exception {
        when(authenticationController.shouldAttemptAuthentication(request)).thenReturn(false);
        doAnswer(new FilterChainInvocation() {
            @SuppressWarnings("deprecation")
            protected void chainInvoked(HttpServletRequest request, HttpServletResponse response) {
                response.setStatus(HttpServletResponse.SC_UNAUTHORIZED, "Denied, Sucka!");
            }
        }).when(chain).doFilter(isA(HttpServletRequest.class), isA(HttpServletResponse.class));

        filter.doFilter(request, response, chain);

        verify(response).addHeader(eq("WWW-Authenticate"), startsWith("OAuth"));
    }

    @Test
    public void verifyWWWAuthenticateHeaderAddedWhenUnauthorizedErrorIsSentWithoutAMessage() throws Exception {
        when(authenticationController.shouldAttemptAuthentication(request)).thenReturn(false);
        doAnswer(new FilterChainInvocation() {
            protected void chainInvoked(HttpServletRequest request, HttpServletResponse response) throws IOException {
                response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
            }
        }).when(chain).doFilter(isA(HttpServletRequest.class), isA(HttpServletResponse.class));

        filter.doFilter(request, response, chain);

        verify(response).addHeader(eq("WWW-Authenticate"), startsWith("OAuth"));
    }

    @Test
    public void verifyWWWAuthenticateHeaderAddedWhenUnauthorizedErrorIsSentWithAMessage() throws Exception {
        when(authenticationController.shouldAttemptAuthentication(request)).thenReturn(false);
        doAnswer(new FilterChainInvocation() {
            protected void chainInvoked(HttpServletRequest request, HttpServletResponse response) throws IOException {
                response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Denied, Sucka!");
            }
        }).when(chain).doFilter(isA(HttpServletRequest.class), isA(HttpServletResponse.class));

        filter.doFilter(request, response, chain);

        verify(response).addHeader(eq("WWW-Authenticate"), startsWith("OAuth"));
    }

    @Test
    public void verifyThatWeStopTheFilterChainAndReportFailureIfAuthenticationFails() throws Exception {
        when(authenticationController.shouldAttemptAuthentication(request)).thenReturn(true);
        Result.Failure failure = new Result.Failure(new OAuthProblem.InvalidToken("1234"));
        when(authenticator.authenticate(isA(HttpServletRequest.class), isA(HttpServletResponse.class))).thenReturn(failure);
        when(request.getParameterMap()).thenReturn(OAUTH_PARAMETERS);
        when(request.getParameter(OAUTH_TOKEN)).thenReturn(OAUTH_PARAMETERS.get(OAUTH_TOKEN)[0]);
        when(request.getRequestURI()).thenReturn("http://sp/service");
        when(request.getContextPath()).thenReturn("");
        filter.doFilter(request, response, chain);

        verify(authenticationListener).authenticationFailure(eq(failure), isA(HttpServletRequest.class), isA(HttpServletResponse.class));
        verifyNoMoreInteractions(authenticationListener);
        verifyZeroInteractions(chain);
    }

    @Test
    public void verifyThatWeStopTheFilterChainAndReportFailureIfThereIsAnErrorDuringAuthentication() throws Exception {
        when(authenticationController.shouldAttemptAuthentication(request)).thenReturn(true);
        Result.Error error = new Result.Error(new OAuthProblem.System(new IOException()));
        when(authenticator.authenticate(isA(HttpServletRequest.class), isA(HttpServletResponse.class))).thenReturn(error);
        when(request.getParameterMap()).thenReturn(OAUTH_PARAMETERS);
        when(request.getParameter(OAUTH_TOKEN)).thenReturn(OAUTH_PARAMETERS.get(OAUTH_TOKEN)[0]);
        when(request.getRequestURI()).thenReturn("http://sp/service");
        when(request.getContextPath()).thenReturn("");
        filter.doFilter(request, response, chain);

        verify(authenticationListener).authenticationError(eq(error), isA(HttpServletRequest.class), isA(HttpServletResponse.class));
        verifyNoMoreInteractions(authenticationListener);
        verifyZeroInteractions(chain);
    }

    @Test
    public void verifyThatAuthenticationControllerIsNotifiedAndFilterChainContinuesWhenAuthenticationIsSuccessful() throws Exception {
        when(authenticationController.shouldAttemptAuthentication(request)).thenReturn(true);
        Result.Success success = new Result.Success(USER);
        when(authenticator.authenticate(isA(HttpServletRequest.class), isA(HttpServletResponse.class))).thenReturn(success);
        when(request.getParameterMap()).thenReturn(OAUTH_PARAMETERS);
        when(request.getParameter(OAUTH_TOKEN)).thenReturn(OAUTH_PARAMETERS.get(OAUTH_TOKEN)[0]);
        when(request.getRequestURI()).thenReturn("http://sp/service");
        when(request.getContextPath()).thenReturn("");

        filter.doFilter(request, response, chain);

        verify(authenticationListener).authenticationSuccess(eq(success), isA(HttpServletRequest.class), isA(HttpServletResponse.class));
        verify(chain).doFilter(isA(HttpServletRequest.class), isA(HttpServletResponse.class));
        verifyNoMoreInteractions(authenticationListener);
    }

    @Test
    public void verifyThatRequestSessionIsInvalidatedFor3LOAuthAccessAttempts() throws Exception {
        when(authenticationController.shouldAttemptAuthentication(request)).thenReturn(true);
        Result.Success success = new Result.Success(USER);
        when(authenticator.authenticate(isA(HttpServletRequest.class), isA(HttpServletResponse.class))).thenReturn(success);
        when(request.getParameterMap()).thenReturn(OAUTH_PARAMETERS);
        when(request.getParameter(OAUTH_TOKEN)).thenReturn(OAUTH_PARAMETERS.get(OAUTH_TOKEN)[0]);
        when(request.getRequestURI()).thenReturn("http://sp/service");
        when(request.getContextPath()).thenReturn("");
        HttpSession session = mock(HttpSession.class);
        when(request.getSession()).thenReturn(session);
        when(request.getSession(false)).thenReturn(session);

        filter.doFilter(request, response, chain);

        verify(session).invalidate();
    }

    @Test
    public void verifyThatRequestSessionIsInvalidatedFor2LOAuthAccessAttempts() throws Exception {
        when(authenticationController.shouldAttemptAuthentication(request)).thenReturn(true);
        Result.Success success = new Result.Success(USER);
        when(authenticator.authenticate(isA(HttpServletRequest.class), isA(HttpServletResponse.class))).thenReturn(success);
        when(request.getParameterMap()).thenReturn(OAUTH_PARAMETERS);
        when(request.getParameter(OAUTH_TOKEN)).thenReturn("");
        when(request.getRequestURI()).thenReturn("http://sp/service");
        when(request.getContextPath()).thenReturn("");
        HttpSession session = mock(HttpSession.class);
        when(request.getSession()).thenReturn(session);
        when(request.getSession(false)).thenReturn(session);

        filter.doFilter(request, response, chain);

        verify(session).invalidate();
    }

    @Test
    public void verifyThatRequestSessionIsNotInvalidatedForNonOAuthAccessAttempts() throws Exception {
        when(authenticationController.shouldAttemptAuthentication(request)).thenReturn(true);
        HttpSession session = mock(HttpSession.class);
        when(request.getSession()).thenReturn(session);
        when(request.getSession(false)).thenReturn(session);

        filter.doFilter(request, response, chain);

        verify(session, never()).invalidate();
    }

    private static abstract class FilterChainInvocation implements Answer<Object> {
        public Object answer(InvocationOnMock invocation) throws Throwable {
            HttpServletRequest request = (HttpServletRequest) invocation.getArguments()[0];
            HttpServletResponse response = (HttpServletResponse) invocation.getArguments()[1];
            chainInvoked(request, response);
            return null;
        }

        protected abstract void chainInvoked(HttpServletRequest request, HttpServletResponse response) throws IOException;
    }
}
